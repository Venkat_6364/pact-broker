# Pact Broker

A simple Docker image for https://github.com/bethesque/pact_broker

```helm upgrade pact-broker charts/pact-broker/ --set ImageTag=latest,IngressDomain=tooling --install --namespace tooling```